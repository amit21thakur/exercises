import {Input, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pets',
  templateUrl: './pets.component.html',
  styleUrls: ['./pets.component.css']
})
export class PetsComponent implements OnInit {

  private _pets;
  get pets() {
    return this._pets;
  }
  @Input()
  set pets(pets) {
    this._pets = pets;
  }
  
  constructor() { }

  ngOnInit() {
  }

}

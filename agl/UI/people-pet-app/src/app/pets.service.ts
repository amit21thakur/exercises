import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PetsService{
    constructor(private http: HttpClient)    { }

    getPetsData(){
        return this.http.get('http://localhost:20298/api/pets');
    }
}
import { Component } from '@angular/core';
import { PetsService } from './pets.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [PetsService]
})
export class AppComponent {
  title = 'app';
  
  private _petsForGender;
  public set petsForGender(data)
  {
    this._petsForGender = data;
    this.HasError = false;
  }
  public get petsForGender()
  {
    return this._petsForGender;
  }


  private _hasError;
  public get HasError()
  {
    return this._hasError;
  }
  public set HasError(hasError)
  {
    this._hasError = hasError;
  }


  constructor(private petsService: PetsService)
  {
    this.petsService.getPetsData().subscribe(
      (response) => {
        this.petsForGender = response;
      },
      (err) => {
        console.log(err)
        this.HasError = true},

    );
  }



}

﻿using System;
using System.Collections.Generic;
using FluentValidation;
using PersonPetsService.Models;

namespace PersonPetsService.Validators
{
    /// <summary>
    /// Validator implementation using FluentValidation
    /// </summary>
    public class PeopleValidator : AbstractValidator<Person>, IValidator<Person>
    {
        public PeopleValidator()
        {
            //Validation rules defined here
            RuleFor(x => x.Age).GreaterThan(0).WithMessage(Constants.AgeGreaterThanZero);

            RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage(Constants.PersonNameEmpty);

            RuleFor(x => x.Gender.ToLower()).Must(IsGenderValid)
                .WithMessage(Constants.InvalidGender);


            RuleForEach(x => x.Pets).Must(p => !string.IsNullOrEmpty(p.Species))
                .WithMessage(Constants.EmptyPetType);

            RuleForEach(x => x.Pets).Must(p => !string.IsNullOrEmpty(p.Name))
                .WithMessage(Constants.EmptyPetName);

        }

        public bool Validate(List<Person> listToValidate, out IList<string> errors)
        {
            errors = new List<string>();
            foreach (var person in listToValidate)
            {
                var validationResults = Validate(person);
                if (!validationResults.IsValid)
                {
                    foreach (var error in validationResults.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                    return false;
                }
            }
            return true;
        }

        //At the moment, we are supporting only two genders
        private bool IsGenderValid(string gender)
        {
            return string.Compare(gender, Constants.Male, StringComparison.InvariantCultureIgnoreCase) == 0 ||
                   string.Compare(gender, Constants.Female, StringComparison.InvariantCultureIgnoreCase) == 0;
        }

    }
}

﻿using System.Collections.Generic;

namespace PersonPetsService.Validators
{
    public interface IValidator<T>
    {
        /// <summary>
        /// Validates the List of given type 
        /// </summary>
        /// <param name="listToValidate">list of objects to be validated</param>
        /// <param name="errors">list of validation errors</param>
        /// <returns>true, if no errors found. Else false</returns>
        bool Validate(List<T> listToValidate, out IList<string> errors);
    }
}

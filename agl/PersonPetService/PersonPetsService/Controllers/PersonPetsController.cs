﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PersonPetsService.DataLoaders;
using PersonPetsService.Models;
using PersonPetsService.Validators;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PersonPetsService.Processor;

namespace PersonPetsService.Controllers
{
    [Route("api/pets")]
    public class PersonPetsController : Controller
    {
        private readonly IDataLoader<Person> _dataLoader;
        private readonly IValidator<Person> _validator;

        /// <summary>
        /// Used to process the bussiness logic
        /// </summary>
        private readonly IProcessor _processor;

        private readonly ILogger _logger;


        public PersonPetsController(IDataLoader<Person> dataLoader, IValidator<Person> validator, IProcessor processor, ILogger<PersonPetsController> logger )
        {
            _dataLoader = dataLoader;
            _validator = validator;
            _processor = processor;
            _logger = logger; 
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            //Load the input data 
            var persons = await _dataLoader.LoadData();

            //Validate the loaded data
            IList<string> validationErrors;
            var isValid = _validator.Validate(persons, out validationErrors);

            if (!isValid)
            {
                //Log validation erros if data is not valid
                foreach (var validationError in validationErrors)
                {
                    _logger.LogError(validationError);
                }
                //Return Http Internal server error
                return StatusCode(500);
            }
            //Process the validated data as per business logic.
            var ownerPetsData = _processor.Process(persons);
            return Ok(ownerPetsData);
        }

    }
}

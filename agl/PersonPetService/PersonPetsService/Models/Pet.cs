﻿using Newtonsoft.Json;

namespace PersonPetsService.Models
{
    public class Pet
    {
        public string Name { get; set; }

        [JsonProperty("type")]
        public string Species { get; set; }

    }
}

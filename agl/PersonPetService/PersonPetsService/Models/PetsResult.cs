﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonPetsService.Models
{
    public class PetsResult
    {
        public string OwnerGender { get; set; }

        public List<string> PetNames { get; set; }

        public bool Equals(PetsResult petsResult)
        {
            var areSame = petsResult != null && petsResult.OwnerGender == OwnerGender;
            if (areSame)
            {
                if (petsResult.PetNames.Count != PetNames.Count)
                    return false;

                for ( int i=0; i< PetNames.Count; i++)
                {
                    areSame = areSame && petsResult.PetNames[i] == PetNames[i];
                }
            }
            return areSame;
        }
    }
}

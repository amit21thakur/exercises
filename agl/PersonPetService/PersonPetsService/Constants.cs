﻿namespace PersonPetsService
{
    public static class Constants
    {
        public const string AgeGreaterThanZero = "The Age must be at greater than 0.";

        public const string PersonNameEmpty = "The Person Name cannot be empty.";

        public const string InvalidGender = "The Gender must have a valid value.";

        public const string EmptyPetType = "The pet type cannot be empty";

        public const string EmptyPetName = "The pet name cannot be empty";

        public const string Male = "Male";

        public const string Female = "Female";

        public const string Logging = "Logging";

        public const string SeriLogFilePath = "SeriLogFilePath";

        public const string JsonWebServiceUrl = "JsonWebServiceUrl";

        public const string CorsUrl = "CorsUrl";
    }
}

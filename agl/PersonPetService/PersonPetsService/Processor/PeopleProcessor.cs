﻿using System.Collections.Generic;
using System.Linq;
using PersonPetsService.Models;
using System.Text.Transformation;

namespace PersonPetsService.Processor
{
    public class PeopleProcessor : IProcessor
    {
        public List<PetsResult> Process(List<Person> persons)
        {
            var ownerPetsData =
                (from person in persons.Where(p => p.Pets != null)
                    from pet in person.Pets.Where(p => p.Species.Trim().ToLower() == "cat")
                    select new
                    {
                        Gender = person.Gender.ToLower().ToTitleCase(),
                        PetName = pet.Name.ToLower().ToTitleCase()
                    })
                .OrderByDescending(d => d.Gender)
                .ThenBy(d => d.PetName)
                .GroupBy(p => p.Gender, p => p.PetName,
                    (ownerGender, petNames) => new PetsResult()
                    {
                        OwnerGender = ownerGender,
                        PetNames = petNames.ToList()
                    }).ToList();

            if (ownerPetsData.Count < 2)
            {
                if (ownerPetsData.Count == 1)
                {
                    var isExistingItemForMale = ownerPetsData[0].OwnerGender == Constants.Male;
                    ownerPetsData.Insert(
                        isExistingItemForMale ? 1 : 0,
                        new PetsResult
                        {
                            OwnerGender = isExistingItemForMale ? Constants.Female : Constants.Male,
                            PetNames = new List<string>()
                        });
                }
                else
                {
                    ownerPetsData.Add(new PetsResult {OwnerGender = Constants.Male, PetNames = new List<string>()});
                    ownerPetsData.Add(new PetsResult {OwnerGender = Constants.Female, PetNames = new List<string>()});
                }
            }
            return ownerPetsData;
        }
    }
}

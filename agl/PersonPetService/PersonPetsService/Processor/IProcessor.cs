﻿using System.Collections.Generic;
using PersonPetsService.Models;

namespace PersonPetsService.Processor
{
    public interface IProcessor
    {
        List<PetsResult> Process(List<Person> persons);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PersonPetsService.Models;

namespace PersonPetsService.DataLoaders
{
    /// <summary>
    /// Implementation of data loader from a web service
    /// </summary>
    public class WebServiceDataLoader: IDataLoader<Person>
    {
        private readonly Uri _uri;
        public WebServiceDataLoader(Uri uri)
        {
            _uri = uri;
        }
        public async Task<List<Person>> LoadData()
        {
            var client = new HttpClient
            {
                BaseAddress = _uri
            };
            var response = await client.GetAsync(String.Empty);

            var persons = new List<Person>();
            if (response.IsSuccessStatusCode)
            {
                var data = await response.Content.ReadAsStringAsync();
                if(data.Length > 0)
                persons = JsonConvert.DeserializeObject<List<Person>>(data);
            }
            return persons;
        }
    }
}

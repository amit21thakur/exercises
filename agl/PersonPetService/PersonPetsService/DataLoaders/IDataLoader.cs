﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace PersonPetsService.DataLoaders
{
    /// <summary>
    /// Data loader interface to support data from any data source
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDataLoader<T>
    {
        Task<List<T>> LoadData();
    }
}

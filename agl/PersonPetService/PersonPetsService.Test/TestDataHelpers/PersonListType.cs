﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonPetsService.Test.TestDataHelpers
{
    public enum PersonListType
    {
        AllGoodData,
        EmptyList,
        IrregularFontCase,
        MissingPetName,
        AgeLessThanOne,
        InvalidGender,
        MissingPetType,
        DuplicatePetNames,
        PetNamesWithSpaces,
        IrregularGenderCase,
        NoPets
    }
}

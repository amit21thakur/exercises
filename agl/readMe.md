# How to run the application locally
The application is build on a 64 bit Windows machine.

## Running Web API Server 

	 1. Install Visual Studio 2017 from https://docs.microsoft.com/en-us/visualstudio/install/install-visual-studio
	 2. Make sure PersonPetsService project is set as Startup Project.
	 3. Press F5, this will host the web API on IIS Express and open web page http://localhost:20298/api/pets

---
## To Open Web client

	 1. Install VS Code from https://code.visualstudio.com/download
	 2. Install Node.js from https://nodejs.org/en/ if not already installed on the machine.
	 3. Open exercises\agl\UI\people-pet-app folder in VS Code

    Run the following commands under Terminal window of VS Code (View >> Integrated Terminal)
	    4. Updating Global project package
		i. npm uninstall -g @angular/cli        (To uninstall the existing angular cli version if already installed on the machine)
	 	ii. npm cache clean                     (To clean the cache - if angular cli is already installed)
	 	iii. npm install -g @angular/cli@latest (To install the latest version of angular cli)
	    5. Updating Local project package
		 i. rm -r -fo node_modules,dist
	 	ii. npm install --save-dev @angular/cli@latest
	 	iii. npm install
	    6. ng serve --port 4200
	    7. Open http://localhost:4200/ in web browser.

---
# Description of the solution
The server part of the application is build using ASP.NET CORE WEB API. 

## Web API design
The constructor of the controller accepts four objects of following type:

    1. IDataLoader
    2. IValidator
    3. IProcessor
    4. ILogger

These interface implementations are injected at runtime. AutoFac Dependency Injection NuGet package is used for this purpose.
With this if tomorrow we have a change in requirement to load the data from some other data source, we have to just implement a new class which implements the IDataLoader interface and change the Dependency Injection setting to use the new class type.


For Validation we are using FluentValidation NuGet package, where we define all the validation rules at one place. A separate implementation of this interface, leaves us free to use some other validator tomorrow in case there is some requirement like that.

Same goes for the IProcessor, which contains the business logic implementation and ILogger which can be used to log to any type of sink.

### Test cases 
 Various unit and feature level test cases are implemented. NuGet packages used: <strong>MOQ</strong> - Mocking framework.

## Client design
The front end of the application is a web portal build using latest version of Angular CLI.

A separate service is implemented to make a HTTP call to the Web API to fetch the data. 

PetsComponent is used to display the data for the Male and Female owners.
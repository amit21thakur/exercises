﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneAgent
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public class Car: IVehicle
    {
        public string Company { get; set; }
        public string Model { get; set; }
        public int Make { get; set; }
        public string Type { get; set; }
        public string Color { get; set; }
        public List<string> Pictures { get; set; }
        public decimal Price { get; set; }
        public string Currency { get; set; }
        public string Location { get; set; }
        public bool IsUsed { get; set; }
        public string Description { get; set; }
    }

    public class Bike : IVehicle
    {
        public string Company { get; set; }
        public string Model { get; set; }
        public int Make { get; set; }
        public string Type { get; set; }
        public string Color { get; set; }
        public List<string> Pictures { get; set; }
        public decimal Price { get; set; }
        public string Currency { get; set; }
        public string Location { get; set; }
        public bool IsUsed { get; set; }
        public string Description { get; set; }
    }
    public class Boat: IVehicle {
        public string Company { get; set; }
        public string Model { get; set; }
        public int Make { get; set; }
        public string Type { get; set; }
        public string Color { get; set; }
        public List<string> Pictures { get; set; }
        public decimal Price { get; set; }
        public string Currency { get; set; }
        public string Location { get; set; }
        public bool IsUsed { get; set; }
        public string Description { get; set; }
    }
    public class Truck : IVehicle
    {
        public string Company { get; set; }
        public string Model { get; set; }
        public int Make { get; set; }
        public string Type { get; set; }
        public string Color { get; set; }
        public List<string> Pictures { get; set; }
        public decimal Price { get; set; }
        public string Currency { get; set; }
        public string Location { get; set; }
        public bool IsUsed { get; set; }
        public string Description { get; set; }
    }
    public class Plane : IVehicle
    {
        public string Company { get; set; }
        public string Model { get; set; }
        public int Make { get; set; }
        public string Type { get; set; }
        public string Color { get; set; }
        public List<string> Pictures { get; set; }
        public decimal Price { get; set; }
        public string Currency { get; set; }
        public string Location { get; set; }
        public bool IsUsed { get; set; }
        public string Description { get; set; }
    }

    public interface IVehicle
    {
        string Company { get; set; }
        string Model { get; set; }
        int Make { get; set; }
        string Type { get; set; }
        string Color { get; set; }
        List<string> Pictures { get; set; }
        decimal Price { get; set; }
        string Currency { get; set; }
        string Location { get; set; }
        bool IsUsed { get; set; }
        string Description { get; set; }

    }


    public interface IFacets
    {
        List<IPropertyFacet> GetFacets();
    }

    public interface IPropertyFacet
    {
        string PropertyName { get; set; }
        List<IFacet> Facets { get; set; }
    }
    public class PropertyFacet : IPropertyFacet
    {
        public string PropertyName { get; set; }
        public List<IFacet> Facets { get; set; }
    }

    public class Facet: IFacet
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }

    public interface IVehicleCollection<T> :IList<T>, ISearch<T>, IFacets, IPagination<T>, IFilter<T>, ISortable<T> where T: IVehicle
    {
        
    }

    public interface ISearch<T>
    {
        List<T> Search(string searchCriteria);
        List<T> ResetSerach();
    }

    public interface IFacet
    {
        string Name { get; set; }
        int Count { get; set; }
    }

    public interface IPagination<T>
    {
        List<T> GetPage(int startIndex, int endIndex);
    }

    public interface IFilter<T>
    {
        List<T> ApplyFilter(string filter);
        List<T> ResetFilter();
    }

    public interface ISortable<T>
    {
        List<T> SortBy(string columnName, bool asc = true);
    }


    public class CarCollection : List<Car>, IVehicleCollection<Car>
    {
        public List<Car> SortBy(string columnName, bool asc = true)
        {
            throw new NotImplementedException();
        }

        public List<Car> Search(string searchCriteria)
        {
            throw new NotImplementedException();
        }

        public List<Car> ResetSerach()
        {
            throw new NotImplementedException();
        }

        public List<IPropertyFacet> GetFacets()
        {
            throw new NotImplementedException();
        }

        public List<Car> GetPage(int startIndex, int endIndex)
        {
            throw new NotImplementedException();
        }

        public List<Car> ApplyFilter(string filter)
        {
            throw new NotImplementedException();
        }

        public List<Car> ResetFilter()
        {
            throw new NotImplementedException();
        }
    }

    public class BikeCollection : List<Bike>, IVehicleCollection<Bike>
    {
        public List<Bike> Search(string searchCriteria)
        {
            throw new NotImplementedException();
        }

        public List<Bike> ResetSerach()
        {
            throw new NotImplementedException();
        }

        public List<IPropertyFacet> GetFacets()
        {
            throw new NotImplementedException();
        }

        public List<Bike> GetPage(int startIndex, int endIndex)
        {
            throw new NotImplementedException();
        }

        public List<Bike> ApplyFilter(string filter)
        {
            throw new NotImplementedException();
        }

        public List<Bike> ResetFilter()
        {
            throw new NotImplementedException();
        }

        public List<Bike> SortBy(string columnName, bool asc = true)
        {
            throw new NotImplementedException();
        }
    }
    public class BoatCollection : List<Boat>, IVehicleCollection<Boat>
    {
        public List<Boat> Search(string searchCriteria)
        {
            throw new NotImplementedException();
        }

        public List<Boat> ResetSerach()
        {
            throw new NotImplementedException();
        }

        public List<IPropertyFacet> GetFacets()
        {
            throw new NotImplementedException();
        }

        public List<Boat> GetPage(int startIndex, int endIndex)
        {
            throw new NotImplementedException();
        }

        public List<Boat> ApplyFilter(string filter)
        {
            throw new NotImplementedException();
        }

        public List<Boat> ResetFilter()
        {
            throw new NotImplementedException();
        }

        public List<Boat> SortBy(string columnName, bool asc = true)
        {
            throw new NotImplementedException();
        }
    }
    public class TruckCollection : List<Truck>, IVehicleCollection<Truck>
    {
        public List<Truck> Search(string searchCriteria)
        {
            throw new NotImplementedException();
        }

        public List<Truck> ResetSerach()
        {
            throw new NotImplementedException();
        }

        public List<IPropertyFacet> GetFacets()
        {
            throw new NotImplementedException();
        }

        public List<Truck> GetPage(int startIndex, int endIndex)
        {
            throw new NotImplementedException();
        }

        public List<Truck> ApplyFilter(string filter)
        {
            throw new NotImplementedException();
        }

        public List<Truck> ResetFilter()
        {
            throw new NotImplementedException();
        }

        public List<Truck> SortBy(string columnName, bool asc = true)
        {
            throw new NotImplementedException();
        }
    }
    public class PlaneCollection : List<Plane>, IVehicleCollection<Plane>
    {
        public List<Plane> Search(string searchCriteria)
        {
            throw new NotImplementedException();
        }

        public List<Plane> ResetSerach()
        {
            throw new NotImplementedException();
        }

        public List<IPropertyFacet> GetFacets()
        {
            throw new NotImplementedException();
        }

        public List<Plane> GetPage(int startIndex, int endIndex)
        {
            throw new NotImplementedException();
        }

        public List<Plane> ApplyFilter(string filter)
        {
            throw new NotImplementedException();
        }

        public List<Plane> ResetFilter()
        {
            throw new NotImplementedException();
        }

        public List<Plane> SortBy(string columnName, bool asc = true)
        {
            throw new NotImplementedException();
        }
    }

}

